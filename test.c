#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <mpi.h>

int comm_size;
int rank;

#define EDGE_SIZE (1 << 18)
#define SIZE (1 << 18)
#define BUFFER_SIZE (1 << 10)
#define HALF_BUFFER_SIZE (BUFFER_SIZE / 2)
#define LOGE(...) fprintf(stderr, __VA_ARGS__)

typedef struct edge_s {
  uint64_t vid : 48;
  uint32_t pid : 16;
} edge_t;

typedef struct edge_pair_s {
  edge_t v0, v1;
} edge_pair_t;

int min(int x, int y)
{
    return x < y ? x : y;
}

int main(int argc, char const *argv[])
{
    MPI_Init(&argc, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int it;
    //local parent
    edge_t* prt = malloc(sizeof(edge_t) * SIZE);
    //init random
    for (it = 0; it < SIZE; ++it)
    {
        prt[it].pid = rand() % comm_size;
        prt[it].vid = rand() % SIZE;
    }
    //local dpt
    uint8_t* dpt = malloc(sizeof(uint8_t) * SIZE);
    //init -1
    for (it = 0; it < SIZE; ++it)
        dpt[it] = rand();//UINT8_MAX
    //local buffer for get
    uint8_t* buffer_dpt = malloc(sizeof(uint8_t) * BUFFER_SIZE);
    for (it = 0; it < BUFFER_SIZE; ++it)
        dpt[it] = UINT8_MAX;

    LOGE("phase 1 begins\n");
    //phase 1
    {
        MPI_Win gather_win;
        MPI_Win_create(&dpt[0], sizeof(uint8_t) * SIZE, sizeof(uint8_t), MPI_INFO_NULL, MPI_COMM_WORLD, &gather_win);

        int ii;
        for (ii = 0; ii < SIZE; ii += BUFFER_SIZE)
        {
            int i_sta = min(ii, SIZE);
            int i_end = min(ii + BUFFER_SIZE, SIZE);
            LOGE("%d %d\n", i_sta, i_end);
            //get from remote windows
            MPI_Win_fence(0, gather_win);
            int i;
            for (i = i_sta; i < i_end; ++i)
            {
                MPI_Get(&buffer_dpt[i-i_sta], 1, MPI_UINT8_T, prt[i].pid, prt[i].vid, 1, MPI_UINT8_T, gather_win);
            }
            MPI_Win_fence(0, gather_win);
            //verify in local buffer
            for (i = i_sta; i < i_end; ++i)
            {
                //random number of get failed to be correct
                if (0)//(UINT8_MAX != buffer_dpt[i-i_sta])
                {
                    fprintf(stderr, "??? %d\n", (int)buffer_dpt[i-i_sta]);
                }
                /*
                else
                {
                    fprintf(stderr, "!!!\n");
                }
                */
            }
        }
        MPI_Win_free(&gather_win);
    }
#if 0
    LOGE("allocate edge_pair_t* edgelist\n");
    edge_pair_t* edgelist = malloc(sizeof(edge_pair_t) * EDGE_SIZE);
    for (it = 0; it < EDGE_SIZE; ++it)
    {
        edgelist[it].v0.pid = rand() % comm_size;
        edgelist[it].v0.vid = rand() % SIZE;
        edgelist[it].v1.pid = edgelist[it].v0.pid;
        edgelist[it].v1.vid = edgelist[it].v0.vid;
    }
    LOGE("phase 2 begins\n");
    //phase 2
    {
        MPI_Win gather_win;
        MPI_Win_create(&dpt[0], sizeof(uint8_t) * SIZE, sizeof(uint8_t), MPI_INFO_NULL, MPI_COMM_WORLD, &gather_win);
        int ii;
        for (ii = 0; ii < EDGE_SIZE; ii += HALF_BUFFER_SIZE)
        {
            int i_sta = min(SIZE, ii);
            int i_end = min(SIZE, ii + HALF_BUFFER_SIZE);
            //get from remote window
            MPI_Win_fence(0, gather_win);
            int i;
            for (i = i_sta; i < i_end; ++i)
            {
                edge_t src = edgelist[i].v0;
                edge_t tgt = edgelist[i].v1;
                MPI_Get(&buffer_dpt[(i-i_sta)*2+0], 1, MPI_UINT8_T, src.pid, src.vid, 1, MPI_UINT8_T, gather_win);
                MPI_Get(&buffer_dpt[(i-i_sta)*2+1], 1, MPI_UINT8_T, tgt.pid, tgt.vid, 1, MPI_UINT8_T, gather_win);
            }
            MPI_Win_fence(0, gather_win);
            //verify in local buffer
            for (i = i_sta; i < i_end; ++i)
            {
                #if 0
                edge_t src = edgelist[i].v0;
                edge_t tgt = edgelist[i].v1;
                #endif
                uint8_t src_dpt = buffer_dpt[(i-i_sta)*2+0];
                uint8_t tgt_dpt = buffer_dpt[(i-i_sta)*2+1];
                if (src_dpt != tgt_dpt) //(!(UINT8_MAX == src_dpt && UINT8_MAX == tgt_dpt))
                {
                    fprintf(stderr, "Phase 2 failed!\n");
                }
            }
        }
        MPI_Win_free(&gather_win);
    }
#endif
    MPI_Finalize();
    return 0;
}
/*
/usr/sz/mpi2/bin/mpicc -Wall -O3 test.c
srun -b -N 4 -n 64 ./a.out
1<<14=32768
export MV2_DEFAULT_PUT_GET_LIST_SIZE=32768
1. iteration variable overflow edge count maxi edge
2. too much traffic->fail after some itewation
3. not correct result?
*/
