#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <mpi.h>

typedef struct {
    void* input;
    size_t input_count;

    size_t elt_size;

    void* output;
    size_t output_count;

    size_t nrequests_max;

    MPI_Datatype datatype;
    MPI_Comm comm;
    int comm_size;

    size_t* local_indices;
    int* remote_ranks;
    size_t* remote_indices;

    int* send_counts;
    int* send_offsets;
    int* recv_counts;
    int* recv_offsets;

    int valid;
} gather_t;

void init_gather(
    void* input, size_t input_count, size_t elt_size,
    void* output, size_t output_count,
    size_t nrequests_max, MPI_Datatype dt, MPI_Comm comm,
    gather_t* g) {
    g = (gather_t*)malloc(sizeof(gather_t));

    g->input = input;
    g->input_count = input_count;

    g->elt_size = elt_size;

    g->output = output;
    g->output_count = output_count;

    g->datatype = dt;
    MPI_Comm_dup(comm, &g->comm);
    g->nrequests_max = nrequests_max;

    g->valid = 0;

    g->local_indices = (size_t*)malloc(nrequests_max * sizeof(size_t));
    g->remote_ranks = (int*)malloc(nrequests_max * sizeof(int));
    g->remote_indices = (size_t*)malloc(nrequests_max * sizeof(size_t));

    MPI_Comm_size(g->comm, &g->comm_size);
    int size = g->comm_size;

    g->send_counts = (int*)malloc((size + 1) * sizeof(int));
    g->send_offsets = (int*)malloc((size + 2) * sizeof(int));
    g->recv_counts = (int*)malloc(size * sizeof(int));
    g->recv_offsets = (int*)malloc((size + 1) * sizeof(int));

    return;
}

void destroy_gather(gather_t* g) {
    assert (!g->valid);
    free(g->local_indices); g->local_indices = NULL;
    free(g->remote_ranks); g->remote_ranks = NULL;
    free(g->remote_indices); g->remote_indices = NULL;
    MPI_Comm_free(&g->comm);
    free(g->send_counts); g->send_counts = NULL;
    free(g->send_offsets); g->send_offsets = NULL;
    free(g->recv_counts); g->recv_counts = NULL;
    free(g->recv_offsets); g->recv_offsets = NULL;
    g->input = NULL;
    g->output = NULL;
    g->input_count = 0;
    g->output_count = 0;
    free(g);
}

void begin_gather(gather_t* g) {
    //assert (!g->valid);
    size_t i, nr = g->nrequests_max;
    for (i = 0; i < nr; ++i)
        g->remote_ranks[i] = g->comm_size;

    g->valid = 1;
}

void add_gather_request(gather_t* g, size_t local_idx, int remote_rank, size_t remote_idx, size_t req_id) {
    //local_idx remote_idx
    assert (g->valid);
    assert (remote_rank >= 0 && remote_rank < g->comm_size);
    assert (req_id < g->nrequests_max);
    assert (local_idx < g->output_count);
    g->local_indices[req_id] = local_idx;
    assert (g->remote_ranks[req_id] == g->comm_size);
    g->remote_ranks[req_id] = remote_rank;
    g->remote_indices[req_id] = remote_idx;
}

void histogram_sort_size_t(int* /*restrict*/ keys,
        const int* /*restrict*/ rowstart,
        int numkeys,
        size_t* /*restrict*/ values1,
        size_t* /*restrict*/ values2) {
    int* /*restrict*/ insert_positions = (int*)malloc(numkeys * sizeof(int));
    memcpy(insert_positions, rowstart, numkeys * sizeof(int));
    int i;
    for (i = 0; i < rowstart[numkeys]; ++i) {
        int key = keys[i];
        assert (key >= 0 && key < numkeys);
        // fprintf(stderr, "i = %d, key = %d\n", i, key);
        while (!(i >= rowstart[key] && i < insert_positions[key])) {
            int target_pos = insert_positions[key]++;
            // fprintf(stderr, "target_pos = %d from limit %d\n", target_pos, rowstart[key + 1]);
            if (target_pos == i) continue;
            assert (target_pos < rowstart[key + 1]);
            // fprintf(stderr, "swapping [%d] = (%d, %d, %d) with [%d] = (%d, %d, %d)\n", i, keys[i], (int)values1[i], (int)values2[i], target_pos, keys[target_pos], (int)values1[target_pos], (int)values2[target_pos]);
            {int t = keys[i]; key = keys[target_pos]; keys[i] = key; keys[target_pos] = t;}
            {size_t t = values1[i]; values1[i] = values1[target_pos]; values1[target_pos] = t;}
            {size_t t = values2[i]; values2[i] = values2[target_pos]; values2[target_pos] = t;}
            assert (key >= 0 && key < numkeys);
        }
    // fprintf(stderr, "done\n");
    }
    for (i = 1; i < rowstart[numkeys]; ++i) {
        assert (keys[i] >= keys[i - 1]);
    }
    free(insert_positions);
}

void end_gather(gather_t* g) {
    assert (g->valid);
    int size = g->comm_size;
    int* /*restrict*/ send_counts = g->send_counts;
    int* /*restrict*/ send_offsets = g->send_offsets;
    int* /*restrict*/ recv_counts = g->recv_counts;
    int* /*restrict*/ recv_offsets = g->recv_offsets;
    size_t* /*restrict*/ local_indices = g->local_indices;
    int* /*restrict*/ remote_ranks = g->remote_ranks;
    size_t* /*restrict*/ remote_indices = g->remote_indices;
    const char* /*restrict*/ input = (const char*)g->input;
    char* /*restrict*/ output = (char*)g->output;
    size_t elt_size = g->elt_size;
    MPI_Comm comm = g->comm;
    MPI_Datatype datatype = g->datatype;
    size_t nrequests_max = g->nrequests_max;

    //send count
    memset(send_counts, 0, (size + 1) * sizeof(int));
    size_t i;
    for (i = 0; i < nrequests_max; ++i) {
        assert (remote_ranks[i] >= 0 && remote_ranks[i] < size + 1);
        ++send_counts[remote_ranks[i]];
    }
    //send count prefix sum
    send_offsets[0] = 0;
    for (i = 0; i < (size_t)size + 1; ++i) {
        assert (send_counts[i] >= 0);
        send_offsets[i + 1] = send_offsets[i] + send_counts[i];
    }
    assert (send_offsets[size + 1] == (int)nrequests_max);

    histogram_sort_size_t(remote_ranks, send_offsets, size + 1, local_indices, remote_indices);
    assert (send_offsets[size] == send_offsets[size + 1] || remote_ranks[send_offsets[size]] == size);
    //sendcount alltoall recvcount
    MPI_Alltoall(send_counts, 1, MPI_INT, recv_counts, 1, MPI_INT, comm);
    //recv count prefix sum
    recv_offsets[0] = 0;
    for (i = 0; i < (size_t)size; ++i) {
        assert (recv_counts[i] >= 0);
        recv_offsets[i + 1] = recv_offsets[i] + recv_counts[i];
    }
    //alloc recv buffer hold indices
    size_t* /*restrict*/ recv_data = (size_t*)malloc(recv_offsets[size] * sizeof(size_t));
    //remoteindices alltoall
    MPI_Alltoallv(remote_indices, send_counts, send_offsets, MPI_UNSIGNED, recv_data, recv_counts, recv_offsets, MPI_UNSIGNED, comm);
    //alloc reply buffer hold get results
    char* /*restrict*/ reply_data = malloc(recv_offsets[size] * elt_size);
    for (i = 0; i < (size_t)recv_offsets[size]; ++i) {
        //assert (recv_data[i] >= 0 && recv_data[i] < input_count);
        memcpy(reply_data + i * elt_size, input + recv_data[i] * elt_size, elt_size);
    }
    free(recv_data);
    char* /*restrict*/ recv_reply_data = malloc(send_offsets[size] * elt_size);
    //reply allto all
    MPI_Alltoallv(reply_data, recv_counts, recv_offsets, datatype, recv_reply_data, send_counts, send_offsets, datatype, comm);
    free(reply_data);
    for (i = 0; i < nrequests_max; ++i) {
        if (remote_ranks[i] >= 0 && remote_ranks[i] < size) {
            memcpy(output + local_indices[i] * elt_size, recv_reply_data + i * elt_size, elt_size);
        }
    }
    free(recv_reply_data);

    g->valid = 0;
}

